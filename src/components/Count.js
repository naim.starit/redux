import React from 'react';
import { connect } from 'react-redux';
const Count = (props) => {
    return (
        <div>
            <h1>I am count {props.count}</h1>
        </div>
    );
}

function mapStatetoProps(state) {
    return {
        count: state.count
    }
}

export default connect(mapStatetoProps)(Count); 
