import React from 'react';
import { Provider } from 'react-redux'
import Controller from './components/Controller';
import Count from './components/Count';
import store from './store'
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <h1>Welcome to you react redux </h1>
          <Count />
          <Controller />
        </div>
      </Provider>
    );
  }
}

export default App;
